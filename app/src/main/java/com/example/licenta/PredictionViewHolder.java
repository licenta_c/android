package com.example.licenta;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PredictionViewHolder extends RecyclerView.ViewHolder {

    private TextView predictionId;
    private TextView predictionLabel;
    private ImageView predictionIcon;

    public PredictionViewHolder(@NonNull View itemView) {
        super(itemView);

        predictionId = itemView.findViewById(R.id.prediction_id);
        predictionLabel = itemView.findViewById(R.id.prediction_label);
        predictionIcon = itemView.findViewById(R.id.prediction_image);
    }

    public TextView getPredictionLabel(){ return predictionLabel;}
    public TextView getPredictionId(){ return predictionId; }
    public ImageView getPredictionIcon() { return predictionIcon; }
}
