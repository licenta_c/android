package com.example.licenta.model;

import java.io.IOException;
import java.net.Socket;

public class Connection {

    public static final String SOCKET_IP = "192.168.1.241";
    public static final String SOCKET_PORT = "9992";
    
    private Socket connection;

    private String ip;
    private String port;

    public Connection(){
        this.ip = SOCKET_IP;
        this.port = SOCKET_PORT;
    }

    public void setUpConnection() {        try {
            connection = new Socket(this.ip, Integer.parseInt(this.port));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() throws IOException {
        connection.close();
    }

    public boolean isConnected(){
        return connection.isConnected();
    }

    public Socket getConnection() throws IOException {
        return connection;
    }
}
