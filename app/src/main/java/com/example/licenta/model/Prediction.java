package com.example.licenta.model;

import java.io.Serializable;

public class Prediction implements Serializable, Cloneable {

    private String fileName;
    private String signLabel;

    public Prediction(String fileName, String signLabel){
        this.fileName = fileName;
        this.signLabel = signLabel;
    }

    public String getFileName(){
        return fileName;
    }

    public String getSignLabel(){
        return signLabel;
    }

    @Override
    public String toString(){
        return String.format("Image: %s\nSign: %s", fileName, signLabel);
    }
}
