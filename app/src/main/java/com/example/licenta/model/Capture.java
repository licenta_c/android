package com.example.licenta.model;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

public class Capture {

    private Bitmap bitmapImage;
    private String file;

    public Capture(Mat image, String file){
        this.file = file;
        setBitmapImage(image);
    }

    public void setBitmapImage(Mat image){
        Bitmap bmp = Bitmap.createBitmap((int)image.size().width, (int)image.size().height, Bitmap.Config.RGB_565);
        Utils.matToBitmap(image, bmp);

        this.bitmapImage = bmp;
    }

    public Bitmap getBitmapImage(){
        return this.bitmapImage;
    }

    public String getFile(){
        return file;
    }
}
