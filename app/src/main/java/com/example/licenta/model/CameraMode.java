package com.example.licenta.model;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;

public class CameraMode {

    private Integer backCameraIndex;
    private Integer frontCameraIndex;

    private Boolean captureReady;
    private Integer currentIndex;

    public CameraMode(CameraManager manager){
        this.findCameras(manager);

        this.captureReady = false;
        this.currentIndex = this.backCameraIndex;
    }

    private void findCameras(CameraManager manager){

        try {
            String[] cameras = manager.getCameraIdList();

            for(String cameraId: cameras){
                CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
                Integer facing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);

                if(facing == CameraMetadata.LENS_FACING_BACK)
                    this.backCameraIndex = Integer.parseInt(cameraId);

                if(facing == CameraMetadata.LENS_FACING_FRONT)
                    this.frontCameraIndex = Integer.parseInt(cameraId);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    public Integer getBackCameraIndex(){
        return this.backCameraIndex;
    }
    
    public Integer switchCamera(){
        if(this.currentIndex == this.backCameraIndex)
            this.currentIndex = this.frontCameraIndex;
        else this.currentIndex = this.backCameraIndex;

        return this.currentIndex;
    }

    public void setCaptureReady(Boolean state){
        this.captureReady = state;
    }

    public Boolean getCaptureReady(){
        return this.captureReady;
    }

}
