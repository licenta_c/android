package com.example.licenta.model;

import android.graphics.Bitmap;

import org.opencv.core.Mat;

public class Result {

    private Capture capture;
    private Prediction prediction;

    public Result(Capture capture, Prediction prediction){
        this.capture = capture;
        this.prediction = prediction;
    }

    public String getFileName(){
        return prediction.getFileName();
    }

    public String getPredictionLabel(){
        return prediction.getSignLabel();
    }
    
    public Bitmap getImage(){
        return capture.getBitmapImage();
    }

}
