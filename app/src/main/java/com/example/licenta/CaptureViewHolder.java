package com.example.licenta;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CaptureViewHolder extends RecyclerView.ViewHolder {

    private ImageView captureIcon;
    private TextView captureId;

    public CaptureViewHolder(@NonNull View itemView) {
        super(itemView);
        captureIcon = (ImageView) itemView.findViewById(R.id.capture_image);
        captureId = (TextView) itemView.findViewById(R.id.capture_name);
    }

    public ImageView getCapture(){
        return captureIcon;
    }
    public TextView getDescription(){
        return captureId;
    }
}
