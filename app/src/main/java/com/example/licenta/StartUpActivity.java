package com.example.licenta;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.licenta.facade.ConnectionCallback;

import java.io.File;

public class StartUpActivity extends AppCompatActivity implements ConnectionCallback<Boolean> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createCacheDirectory("Captures");
        ConnectionRunner connectionRunner = setUpConnection();
        connectionRunner.start();
    }

    private ConnectionRunner setUpConnection(){
        ConnectionRunner connectionRunner = new ConnectionRunner();
        connectionRunner.setCallback(this);

        return connectionRunner;
    }

    private void createCacheDirectory(String dirName){
        File dir = new File(getCacheDir(), dirName);
        dir.mkdir();
    }

    @Override
    public void onComplete(Boolean status) {
        new Handler(Looper.getMainLooper()).post(() -> {
            Toast.makeText(getApplicationContext(), (status)?getString(R.string.connected):getString(R.string.not_connected), Toast.LENGTH_LONG)
                    .show();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        });
    }
}