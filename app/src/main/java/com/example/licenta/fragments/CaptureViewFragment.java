package com.example.licenta.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.licenta.adapter.CaptureAdapter;
import com.example.licenta.R;
import com.example.licenta.utils.AppUtils;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.util.Arrays;

public class CaptureViewFragment extends Fragment {

    private CaptureAdapter captureAdapter;

    BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(getContext()) {
        @Override
        public void onManagerConnected(int status) {

            switch(status){
                case BaseLoaderCallback.SUCCESS: {
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.capture_fragment, container, false);

        if(OpenCVLoader.initDebug())
            baseLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS);
        else
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, getContext(), baseLoaderCallback);


        RecyclerView recyclerView = view.findViewById(R.id.capture_view);

        captureAdapter = new CaptureAdapter(Arrays.asList());
        recyclerView.setAdapter(captureAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        return view;
    }

    public void reloadCaptures(File dir){
        if(captureAdapter != null)
        {
            captureAdapter.setCaptures(AppUtils.loadCaptures(dir));
            captureAdapter.notifyDataSetChanged();
        }
    }
}
