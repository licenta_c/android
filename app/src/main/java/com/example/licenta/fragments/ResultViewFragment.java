package com.example.licenta.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.licenta.adapter.PredictionAdapter;
import com.example.licenta.R;
import com.example.licenta.model.Prediction;

import java.io.File;
import java.util.List;

public class ResultViewFragment extends Fragment {

    private PredictionAdapter predictionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.result_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.result_view);

        predictionAdapter = new PredictionAdapter();
        recyclerView.setAdapter(predictionAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void addPredictions(List<Prediction> predictions, File dir){
        if(predictionAdapter != null)
            predictionAdapter.addPredictions(predictions, dir);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void reloadPredictions(List<Prediction> predictions, File dir){
        if(predictionAdapter!=null) {
            predictionAdapter.addPredictions(predictions, dir);
            predictionAdapter.notifyDataSetChanged();
        }
    }

}