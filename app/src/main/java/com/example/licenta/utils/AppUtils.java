package com.example.licenta.utils;

import com.example.licenta.model.Capture;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AppUtils {

    public static List<Capture> loadCaptures(File dir){

        List<Capture> captures = new ArrayList<Capture>();

        if(dir.exists()){
            for(File f: dir.listFiles())
            {
                Mat savedImg = Imgcodecs.imread(f.getPath());
                captures.add(new Capture(savedImg, f.getName()));
            }
        }

        return captures;
    }
}
