package com.example.licenta.adapter;

import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class CustomViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments = new ArrayList<>();
    private List<String> fragmentsTitle = new ArrayList<>();

    private Resources resources;
    private String packageName;

    public CustomViewPagerAdapter(FragmentManager manager, Resources resources, String packageName){
        super(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        this.resources = resources;
        this.packageName = packageName;
    }

    public void addFragment(Fragment fragment, String fragmentTitle){
        fragments.add(fragment);
        fragmentsTitle.add(fragmentTitle);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        return resources.getString(resources.getIdentifier(fragmentsTitle.get(position),"string",packageName));
    }
}
