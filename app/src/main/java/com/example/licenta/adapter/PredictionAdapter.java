package com.example.licenta.adapter;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.licenta.PredictionViewHolder;
import com.example.licenta.R;
import com.example.licenta.model.Capture;
import com.example.licenta.model.Prediction;
import com.example.licenta.model.Result;
import com.example.licenta.utils.AppUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PredictionAdapter extends RecyclerView.Adapter<PredictionViewHolder> {
    private final List<Result> predictions = new ArrayList<>();

    public PredictionAdapter(){}

    @NonNull
    @Override
    public PredictionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View captureView = inflater.inflate(R.layout.item_prediction, parent, false);

        PredictionViewHolder captureViewHolder = new PredictionViewHolder(captureView);

        return captureViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void addPredictions(List<Prediction> newlyPredictions, File dir){
        List<Capture> captures = AppUtils.loadCaptures(dir);

        for(Prediction p: newlyPredictions) {
            Optional<Capture> capture = captures.stream().filter(c -> c.getFile().split("[.]")[0].equals(p.getFileName())).findFirst();

            if(capture.isPresent())
                this.predictions.add(new Result(capture.get(), p));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PredictionViewHolder holder, int position) {
        Result item = predictions.get(position);

        TextView predictionMeta = holder.getPredictionId();
        predictionMeta.setText(item.getFileName());

        TextView predictionLabel = holder.getPredictionLabel();
        predictionLabel.setText(item.getPredictionLabel());

        ImageView predictionIcon = holder.getPredictionIcon();
        predictionIcon.setImageBitmap(item.getImage());
    }

    @Override
    public int getItemCount() {
        return predictions.size();
    }

}
