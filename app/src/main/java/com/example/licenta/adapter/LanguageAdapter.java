package com.example.licenta.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.licenta.R;
import com.example.licenta.facade.RefreshScreen;

import java.util.List;
import java.util.Locale;

public class LanguageAdapter extends BaseAdapter {

    private RefreshScreen mainScreen;
    private List<Locale> languages;
    private Context context;

    public LanguageAdapter (List<Locale> languages, Context context, RefreshScreen mainScreen){
        this.languages = languages;
        this.context = context;
        this.mainScreen = mainScreen;
    }

    @Override
    public int getCount() {
        return languages.size();
    }

    @Override
    public Object getItem(int position) {
        return languages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Locale itemLocale = (Locale) getItem(position);
        Locale locale = context.getResources().getConfiguration().getLocales().get(0);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.language_fragment, parent, false);

            ImageView languageIcon = convertView.findViewById(R.id.language_icon);

            Resources resources = context.getResources();
            Integer resourceId = resources.getIdentifier("ic_"+itemLocale.getLanguage(), "drawable",context.getPackageName());
            languageIcon.setImageResource(resourceId);
            TextView languageName = convertView.findViewById(R.id.language_descr);
            languageName.setText(itemLocale.getDisplayLanguage());

            convertView.setOnClickListener(v -> {
                context.getResources().getConfiguration().setLocale(itemLocale);
                context.getResources().updateConfiguration(context.getResources().getConfiguration(), context.getResources().getDisplayMetrics());

                mainScreen.changeAppLanguage(resourceId);
            });
        }

        TextView languageItemTextView = convertView.findViewById(R.id.language_descr);

        if (locale.equals(itemLocale))
            languageItemTextView.setTextColor(context.getColor(R.color.black));
        else
            languageItemTextView.setTextColor(context.getColor(R.color.white));

        return convertView;
    }
}
