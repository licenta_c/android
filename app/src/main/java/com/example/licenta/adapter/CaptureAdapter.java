package com.example.licenta.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.licenta.CaptureViewHolder;
import com.example.licenta.R;
import com.example.licenta.model.Capture;

import java.util.List;

public class CaptureAdapter extends RecyclerView.Adapter<CaptureViewHolder> {

    private List<Capture> captures;

    public CaptureAdapter(List<Capture> captures){
        this.captures = captures;
    }

    public void setCaptures(List<Capture> captures){
        this.captures = captures;
    }

    @NonNull
    @Override
    public CaptureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View captureView = inflater.inflate(R.layout.item_caputre, parent, false);

        CaptureViewHolder captureViewHolder = new CaptureViewHolder(captureView);

        return captureViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CaptureViewHolder holder, int position) {
        Capture capture = captures.get(position);

        ImageView captureImg = holder.getCapture();
        captureImg.setImageBitmap(capture.getBitmapImage());

        TextView captureDescription = holder.getDescription();
        captureDescription.setText(capture.getFile());
    }

    @Override
    public int getItemCount() {
        return captures.size();
    }
}
