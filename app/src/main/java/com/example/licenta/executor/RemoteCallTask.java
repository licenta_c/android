package com.example.licenta.executor;

import com.example.licenta.model.Connection;
import com.example.licenta.model.Prediction;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

public class RemoteCallTask implements Callable<Prediction> {

    private Mat frame;
    private String frameName;

    private DataOutputStream outputStream;
    private DataInputStream inputStream;

    public RemoteCallTask(Mat frame, String frameName){

        this.frameName = frameName;
        this.frame = new Mat(frame.size(), CvType.CV_8UC3);
        Imgproc.cvtColor(frame, this.frame, Imgproc.COLOR_RGBA2BGR);

        Socket socketConnection;
        Connection connection = new Connection();
        connection.setUpConnection();

        try {
            socketConnection = connection.getConnection();
            this.outputStream = new DataOutputStream(socketConnection.getOutputStream());
            this.inputStream = new DataInputStream(socketConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Prediction call() throws Exception {

        int size = (int) (frame.total() * frame.channels());
        byte[] serializableMat = new byte[size];
        frame.get(0, 0, serializableMat);

        try{
            outputStream.write(serializableMat);
        }catch(NullPointerException e) {
            System.err.println("Sending error!");
        }

        byte[] signLabel = new byte[1];
        inputStream.read(signLabel,0,1);

        return new Prediction(frameName, new String(signLabel, Charset.forName("UTF-8")));
    }
}
