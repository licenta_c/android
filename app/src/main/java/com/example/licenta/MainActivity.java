package com.example.licenta;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.example.licenta.adapter.CustomViewPagerAdapter;
import com.example.licenta.adapter.LanguageAdapter;
import com.example.licenta.facade.RefreshScreen;
import com.example.licenta.fragments.CaptureViewFragment;
import com.example.licenta.fragments.ResultViewFragment;
import com.example.licenta.model.Prediction;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, RefreshScreen {

    public static final List<Locale> AVAILABLE_LANGUAGES = Arrays.asList(Locale.ENGLISH, Locale.GERMAN, Locale.ITALIAN);
    public static final String CACHE_DIR_NAME = "Captures";

    private MenuItem languageItem;
    private DrawerLayout drawer;
    private ActivityResultLauncher<String> requestPermissonLauncher;

    public CaptureViewFragment captureFragment;
    public ResultViewFragment resultFragment;
    public Boolean soundsOn = true;

    public MainActivity(){
        this.requestPermissonLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if(isGranted)
                startRecordActivity();
            else {
                String permissionMessage = getString(R.string.permission_fail);
                Toast.makeText(this, permissionMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch (item.getItemId()) {
            case R.id.sound: {

                if (item.getTitle().equals("Off")) {
                    item.setIcon(R.drawable.ic_speaker);
                    item.setTitle("On");

                    ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                    toneGen.startTone(ToneGenerator.TONE_CDMA_PIP, 150);

                    soundsOn = true;
                } else {
                    item.setIcon(R.drawable.ic_mute);
                    item.setTitle("Off");

                    soundsOn = false;
                }

                break;
            }
        }
        return true;
    }

    private void setNavigationViewListener () {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void clearCache () {
        File dir = new File(getCacheDir(), CACHE_DIR_NAME);
        if (dir.exists()) {
            for (File f : dir.listFiles())
                f.delete();
        }
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.option_menu, menu);
        this.languageItem = menu.findItem(R.id.app_language);

        return true;
    }

    @Override
    public void onCreate (Bundle instanceBundle) {

        super.onCreate(instanceBundle);
        setContentView(R.layout.activity_start);

        clearCache();
        setUpDrawerMenu();
        setUpPager();
        setNavigationViewListener();
    }

    private void setUpDrawerMenu(){
        Toolbar mTopToolbar = findViewById(R.id.start_toolbar);
        setSupportActionBar(mTopToolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, mTopToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setUpPager(){
        ViewPager viewPager = findViewById(R.id.view_pager_id);
        CustomViewPagerAdapter adapter = new CustomViewPagerAdapter(getSupportFragmentManager(), getResources(), getPackageName());

        captureFragment = new CaptureViewFragment();
        resultFragment = new ResultViewFragment();

        adapter.addFragment(captureFragment, "Captures");
        adapter.addFragment(resultFragment, "Results");

        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_id);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed () {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    public void startRecordActivity () {
        Intent intent = new Intent(getApplicationContext(), RecordActivity.class);

        intent.putExtra("sound_state", soundsOn);
        startActivityForResult(intent, 0);
    }

    public void beginRecord (View component){
        if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            startRecordActivity();
        else
            requestPermissonLauncher.launch(Manifest.permission.CAMERA);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onActivityResult ( int requestCode, int resultCode, Intent data){
        if (requestCode == 0) {
            List<Prediction> predictions = Arrays.asList((Prediction[])data.getSerializableExtra("predictions"));
            File dir = new File(getCacheDir(), CACHE_DIR_NAME);

            resultFragment.reloadPredictions(predictions, dir);
            captureFragment.reloadCaptures(dir);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected (@NonNull MenuItem item){
        switch (item.getItemId()) {
            case R.id.exit: {
                this.finish();
                break;
            }
            case R.id.language: {
                LayoutInflater inflater = LayoutInflater.from(this);
                View view = inflater.inflate(R.layout.language_layout, null);

                LanguageAdapter languageAdapter = new LanguageAdapter(AVAILABLE_LANGUAGES, this, this);
                ListView languageListView = view.findViewById(R.id.languages);
                languageListView.setAdapter(languageAdapter);

                showLanguageWindow(view);
                break;
            }
        }

        return false;
    }

    private void showLanguageWindow(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setLayout(500, 500);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.layout_bg);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void changeAppLanguage(Integer imageId) {

        NavigationMenuItemView exitMenu = findViewById(R.id.exit);
        exitMenu.setTitle(getResources().getString(R.string.exit));
        NavigationMenuItemView languageMenu = findViewById(R.id.language);
        languageMenu.setTitle(getResources().getString(R.string.language));

        setUpPager();

        languageItem.setIcon(imageId);
    }
}
