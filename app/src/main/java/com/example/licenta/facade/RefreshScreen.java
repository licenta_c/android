package com.example.licenta.facade;

public interface RefreshScreen {
    void changeAppLanguage(Integer resourceId);
}
