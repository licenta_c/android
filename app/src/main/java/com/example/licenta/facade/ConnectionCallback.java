package com.example.licenta.facade;

public interface ConnectionCallback<T> {
    void onComplete(T status);
}
