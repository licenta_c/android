package com.example.licenta;

import com.example.licenta.facade.ConnectionCallback;
import com.example.licenta.model.Connection;
import java.io.IOException;

public class ConnectionRunner extends Thread {

    private ConnectionCallback<Boolean> mainThreadCallback;

    public void setCallback(ConnectionCallback<Boolean> mainThreadCallback) {
        this.mainThreadCallback = mainThreadCallback;
    }

    @Override
    public void run() {
        Connection connection = new Connection();
        connection.setUpConnection();;

        try {
            if(connection.isConnected()) {
                mainThreadCallback.onComplete(true);
                connection.closeConnection();
            }
            else mainThreadCallback.onComplete(false);
        } catch (IOException e) {
            mainThreadCallback.onComplete(false);
        }

    }
}
