package com.example.licenta;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;

import androidx.activity.ComponentActivity;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.example.licenta.executor.PredictionService;
import com.example.licenta.executor.RemoteCallTask;
import com.example.licenta.model.CameraMode;
import com.example.licenta.model.Prediction;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class RecordActivity extends ComponentActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    public static final Size IMAGE_SIZE = new Size(300, 300);

    private JavaCameraView cameraView;
    private List<Prediction> predictions = new ArrayList<Prediction>();

    public PredictionService predictionService;
    public CameraMode cameraMode;
    public Boolean soundsOn;

    BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {

            switch(status){
                case BaseLoaderCallback.SUCCESS: {
                    cameraView.enableView();
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle instanceBundle) {
        super.onCreate(instanceBundle);
        setContentView(R.layout.record_activity);

        initExecutor();
        setUpCamera();

        soundsOn = (Boolean) getIntent().getExtras().get("sound_state");
    }

    private void initExecutor(){
        predictionService = new PredictionService(Executors.newFixedThreadPool(8));
    }

    private void setUpCamera(){
        CameraManager cameraManager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        this.cameraMode = new CameraMode(cameraManager);

        cameraView = findViewById(R.id.camera);
        cameraView.setVisibility(SurfaceView.VISIBLE);

        Integer backCameraIndex = this.cameraMode.getBackCameraIndex();
        cameraView.setCameraIndex(backCameraIndex);
        cameraView.setCvCameraViewListener(this);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            cameraView.setCameraPermissionGranted();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat img = inputFrame.rgba();

        if (cameraMode.getCaptureReady()) {

            Mat resizedImg = new Mat(IMAGE_SIZE, CvType.CV_8UC3);
            Imgproc.resize(img, resizedImg, IMAGE_SIZE);
            Date currentTime = Calendar.getInstance().getTime();
            String frameName = "Capture_" + currentTime.toString().substring(10, 19);

            predictionService.submit(new RemoteCallTask(resizedImg, frameName));

            File file = new File(new File(getCacheDir(), "Captures"), frameName + ".png");
            Imgcodecs.imwrite(file.toString(), resizedImg);

            cameraMode.setCaptureReady(false);
        }

        Prediction prediction;

        try {
            Future<Prediction> newPrediction = predictionService.poll(100, TimeUnit.MILLISECONDS);
            if(newPrediction!=null) {
                prediction = newPrediction.get();

                if(prediction!=null)
                    predictions.add(prediction);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return img;
    }

    public void stopRecord(View view){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            packResults();
        }

        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void packResults(){
        Intent data = new Intent();

        Prediction[] predictions = this.predictions.toArray(new Prediction[this.predictions.size()]);
        data.putExtra("predictions", predictions);
        setResult(0, data);
    }

    public void captureShot(View view){
        if(soundsOn) {
            ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen.startTone(ToneGenerator.TONE_CDMA_ANSWER, 150);
        }

        cameraMode.setCaptureReady(true);
    }

    public void switchCamera(View view){
        this.onPause();
        Integer cameraIndex = cameraMode.switchCamera();
        cameraView.setCameraIndex(cameraIndex);
        cameraView.enableView();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(cameraView != null)
            cameraView.disableView();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (cameraView != null)
            cameraView.disableView();
    }

    protected void onResume(){
        super.onResume();

        if(OpenCVLoader.initDebug())
            baseLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS);
        else
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, baseLoaderCallback);
    }
}
